<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Item extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        DB::table("items")->insert([
            "name"  => "Sample Product 1",
            "description" => "A very cool product.",
            "price" => 100.00,
            "currency" => "PHP",
        ]);

        DB::table("items")->insert([
            "name"  => "Sample Product 2",
            "description" => "Another cool product.",
            "price" => 200.00,
            "currency" => "PHP",
        ]);
    }
}
