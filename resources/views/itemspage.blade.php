@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        @for ($i = 0; $i < count($items); $i++)
            <div class="col-sm-6">
                <div class="card m-1">
                <div class="card-body">
                    <h5 class="card-title">{{ $items[$i]['name']}}</h5>
                    <p class="card-text">{{ $items[$i]['description']}}</p>
                    <p class="card-text">{{ $items[$i]['currency']}} {{ number_format($items[$i]['price'],2)}}</p>
                    <larapay-btn itemid="{{ $items[$i]['id']}}"></larapay-btn>
                </div>
                </div>
            </div>
        @endfor

    </div>
</div>
@endsection


