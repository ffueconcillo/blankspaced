# Project: Blankspace


## Technologies:
- [Laravel](https://laravel.com/docs)
- [bitnami/bitnami-docker-laravel](https://github.com/bitnami/bitnami-docker-laravel)


## Local Development Setup
- Install [Docker Desktop](https://docs.docker.com/desktop/)
- Create `.env` file from `.env.example`. Get `.env` values from team members.
- Run application services in Docker
```
$ docker-compose up
```
- Install node dependencies
```
$ docker-compose exec blankspace npm install
```
- Compile Frontend JS and CSS. Keep this command running in a window.
```
$ docker-compose exec blankspace npm run watch
```

## Development How-Tos
- How to install a dependency by Composer
```
$ docker-compose exec blankspace composer require luigel/laravel-paymongo
```
- How to add/apply new `.env` variables
```
docker-compose exec blankspace php artisan config:clear
```
- How restart container
```
$ docker-compose restart blankspace
```
- How to run other commands
```
$ docker-compose exec blankspace php artisan dump-autoload
$ docker-compose exec blankspace php artisan --version
$ docker-compose exec blankspace php artisan
```