<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;
use Luigel\Paymongo\Facades\Paymongo;



class Purchase extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $item = Item::where('id', $id)->get();

        $itemShortDesc = strtr('(@id) @name', [ 
            '@id' => $item[0]['id'], 
            '@name' => $item[0]['name'], 
        ]);

        $paymentIntent = Paymongo::paymentIntent()->create([
            'amount' => number_format($item[0]['price'], 2),  // Amount in cents. https://developers.paymongo.com/reference#create-a-paymentintent
            'payment_method_allowed' => [
                'card'
            ],
            'payment_method_options' => [
                'card' => [
                    'request_three_d_secure' => 'automatic'
                ]
            ],
            'description' => $itemShortDesc,
            'statement_descriptor' => env('PAYMONGO_STATEMENT_DESCRIPTOR', 'LaraPay'),
            'currency' => $item[0]['currency'],  // PayMongo only support PHP at the moment
            'metadata' => [
                'itemid' => $id
            ],
        ]);
        
        return view('purchase', [ 
            'name' => $item[0]['name'],
            'description' => $item[0]['description'],
            'currency' => $item[0]['currency'],
            'price' => strval(number_format($item[0]['price'], 2)),
            'client_key' => $paymentIntent->client_key,
        ]);
    }
}
