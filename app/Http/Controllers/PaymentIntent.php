<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Luigel\Paymongo\Facades\Paymongo;

class PaymentIntent extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function retrieve(Request $request, $id)
    {
        $paymentIntent = Paymongo::paymentIntent()->find($id);
        return 'client-key:'.$paymentIntent->status;
    }
}
