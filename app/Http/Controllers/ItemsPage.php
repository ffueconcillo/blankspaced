<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;

class ItemsPage extends Controller
{
    public function index()
    {
        $items = Item::all();
        return view('itemspage', compact('items'));
    }
}
