<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

# change middleware to `auth` instead of `auth:api` 
# check https://laracasts.com/discuss/channels/laravel/laravel-api-route-redirect-to-home-route
Route::middleware('auth')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/paymentintent/{id}', 'PaymentIntent@retrieve')->name('getpaymentintent');

Route::get('/paymentcallback/{id}', 'PaymentCallback@index')->name('paymentcallback');
